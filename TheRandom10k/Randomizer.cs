﻿using System;
using System.Collections.Generic;

namespace TheRandom10k
{
    /// <summary>
    /// Static class that generates an array of specified size (10,000 by default)
    /// populated random yet unique integers in the specified range of 1 to array size
    /// </summary>
    public static class Randomizer
    {
        // primary array
        private static int[] FINALARRAY;

        // auxilary structures used in generating randomness
        // indexes refers to the indexes in the final array
        // possibilities to the possible values
        private static IList<int> indexes = new List<int>();
        private static IList<int> possibilities = new List<int>();
       
        // size of arrays is shared amongst the primary array and the auxilaries
        private static int SIZEOFARR;

        /// <summary>
        /// publicaly exposed method for generating the array of n random integers
        /// </summary>
        /// <param name="sizeOfArr">how many integers to generate and what size array to put them in</param>
        /// <returns>array of specified size</returns>
        public static int[] Generate(int sizeOfArr = 10000)
        {
            // Array of size 0 or negative size simply makes no sense in this context.
            // Error of this type should be fixed by the API user, my job is to notify them
            if (sizeOfArr < 1)
            {
                throw new ArgumentOutOfRangeException();
            }
            SIZEOFARR = sizeOfArr;

            FINALARRAY = new int[SIZEOFARR];

            populateAuxArrays();
            randomize();

            return FINALARRAY;
        }

        /// <summary>
        /// indexes array goes from 0 to SIZEOFARR
        /// possibilities goes from 1 to SIZEOFARR+1
        /// </summary>
        private static void populateAuxArrays()
        {
            for (int i = 0; i < SIZEOFARR; i++)
            {
                indexes.Add(i);
                possibilities.Add(i + 1);
            }
        }



        #region randomizers

        /// <summary>
        /// The algorithm itself.
        /// Uses two auxilary arrays and a random(-ish) number generator from corelib.
        /// 
        /// Random number generator 'r' will generate two number in range of available indexes of auxilary arrays:
        /// i_rand - to be used for 'index' array
        /// p_rand - to be used for 'possibilities' array
        /// 
        /// The numbers are then used as indexes to extract values in the Lists at those indexes.
        /// Once values are extracted the indexes are cleaned up and the size of aux arrays are reduced by one.
        /// 
        /// </summary>
        private static void randomize()
        {
            int local_size_copy = SIZEOFARR;
            Random r = new Random();
            while (local_size_copy > 0)
            {
                int i_rand = r.Next(0, local_size_copy);
                int p_rand = r.Next(0, local_size_copy);

                int ind = indexes[i_rand];
                indexes.RemoveAt(i_rand);
                int poss = possibilities[p_rand];
                possibilities.RemoveAt(p_rand);

                FINALARRAY[ind] = poss;
                local_size_copy--;
            }
        }

        #endregion

    }
}
