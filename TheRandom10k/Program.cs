﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheRandom10k
{
    class Program
    {
        static void Main(string[] args)
        {
            // example use, default
            var result1 = Randomizer.Generate();

            //example use 10 results
            var result2 = Randomizer.Generate(10);
            
        }
    }
}
